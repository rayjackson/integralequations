package ru.omsu.imit;

import static java.lang.Math.*;
import static ru.omsu.imit.MatrixFunctions.*;

class Quattro {
    /**
     * Функция f(x) = u(x) - λ * ∫K(x, s)u(s)ds
     * @param x
     * @param lambda - λ
     * @return f(x)
     */
    static double function(double x, double lambda) {
        return .5 * x - 1. / 3;
    }

    /**
     * Ядро интегрального уравнения K(x, s)
     */
    private static double kernel(double x, double s) {
        return x + s;
    }

    /**
     * Решение интегрального уравнение Фредгольма 2-го рода методом квадратур.
     * Имеется функция (function), ядро уравнения (kernel) и лямбда-параметр.
     * После преобразования получаем СЛАУ, и вызываем метод Якоби для решения его.
     */
    static double[] integ(double lambda, int size, double eps, double[][] a, double[] f) {
        double[][] e = createUnitMatrix(size);
        double[] x = new double[size]; // xm = x(i - 1/2)
        double[] y = new double[size];
        double h = 1. / size;

        for (int i = 0; i < size; i++) {
            x[i] = (i + .5) * h;
        }

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                a[i][j] = e[i][j] - lambda * h * kernel(x[i], x[j]);
            }
            f[i] = function(x[i], lambda);
        }

        for (int i = 0; i < size; i++) {
            y[i] = x[i]; // u(x) = x
        }

        jacobi(a, f, y, eps);

        return y;
    }

    /**
     * Решение системы линейных уравнений методом Якоби.
     * На выходе получаем вектор x.
     */
    static void jacobi(double[][] A, double[] F, double[] Y, double eps) {
        int size = A.length;
        double[][] triangleMatrix = triangleMatrix(A);
        double[] TempX = new double[size];
        double norm;
        int count = 0;

        do {
            for (int i = 0; i < size; i++) {
                TempX[i] = F[i];
                for (int g = 0; g < size; g++) {
                    if (i != g)
                        TempX[i] -= triangleMatrix[i][g] * Y[g];
                }
                TempX[i] /= triangleMatrix[i][i];
            }
            norm = abs(Y[0] - TempX[0]);
            for (int h = 0; h < size; h++) {
                if (abs(Y[h] - TempX[h]) > norm)
                    norm = abs(Y[h] - TempX[h]);
                Y[h] = TempX[h];
            }
            count++;
        } while (norm > eps);
        A = copyMatrix(triangleMatrix);
        System.out.println("iter = " + count);
    }

}
