package ru.omsu.imit;

import static ru.omsu.imit.Quattro.*;
import static ru.omsu.imit.MatrixFunctions.*;

public class Main {
    private static int SIZE = 100;

    public static void main(String[] args) {
        testInteg();
    }

    public static void testInteg() {
        for (int size = 20; size <= SIZE; size += 20) {
            double eps = 1.e-7;
            testIntegIteration(size, eps);
        }
    }

    /**
     * Тестирование метода квадратур.
     *
     * @param size - размерность,
     * @param eps  - предполагаемая точность ε.
     */
    public static void testIntegIteration(int size, double eps) {
        System.out.println("N = " + size);
        System.out.println("ε = " + eps);
        double[] x; // x приближенное
        double lambda = 1;
        double h = 1. / size;
        double[][] a = new double[size][size];
        double[] f = new double[size];
        x = integ(lambda, size, eps, a, f);

        showError(x);
        showResidual(a, x, f);
        System.out.println();
    }

    /**
     * ------------------------------------------
     * Вывод ошибки z и относительной ошибки ζ.
     * z = x - x*
     * ζ = ||z|| / ||x||
     * ------------------------------------------
     */
    static void showError(double[] x) {
        int size = x.length;
        double h = 1. / size;
        double[] xtoch = new double[size]; // x точное
        for (int i = 0; i < size; i++) {
            xtoch[i] = (i + .5) * h * .5 - 1. / 3;
        }
        double[] z = countError(x, xtoch);
        double normZ = normVector(z);
        double normX = normVector(x);
        double zeta = normZ / normX;
        System.out.println("||z|| = " + normZ);
        System.out.println("zeta = " + zeta);
    }

    /**
     * ------------------------------------------
     * Вывод невязки r и относительной невязки ρ.
     * r = A * x - f
     * ρ = ||r|| / ||f||
     * ------------------------------------------
     */
    static void showResidual(double[][] A, double[] x, double[] f) {
        double normR = normVector(countResidual(A, x, f));
        double normF = normVector(f);
        System.out.println("||R|| = " + normR);
        System.out.println("ro = " + normR / normF);
    }

    private static void testJacobi() {
        Gen gen = new Gen();
        for (double alpha = .1; alpha > 1e-17; alpha /= 10) {
            for (double beta = 1.; beta < 2.; beta++) {
                testJacobiIteration(gen, alpha, beta);
            }
        }
    }

    private static void testJacobiIteration(Gen gen, double alpha, double beta) {
        double[][] A = new double[SIZE][SIZE];
        double[][] A_inv = new double[SIZE][SIZE];

        gen.mygen(A, A_inv, SIZE, alpha, beta, 1, 2, 1, 1);

        double[][] A_copy = copyMatrix(A);

        double h = 1. / SIZE;
        double[] f = new double[SIZE];
        double[] xtoch = new double[SIZE];
        double[] x = new double[SIZE];
        for (int i = 0; i < SIZE; i++) {
            xtoch[i] = Math.sin(Math.PI * Math.PI * h * (i + .5));
        }
        for (int i = 0; i < SIZE; i++) {
            x[i] = 0;
        }
        for (int i = 0; i < SIZE; i++) {
            int LAMBDA = 1;
            f[i] = function(xtoch[i], LAMBDA);
        }

        jacobi(A, f, x, 1e-9);

        // Ошибка
        double[] z = countError(x, xtoch);
        double normZ = normVector(z);
        double normX = normVector(x);
        double zeta = normZ / normX;

        // Невязка
//        double[] r = countResidual(A_copy, z);
        double[] r = countResidual(A, x, f);
        double normR = normVector(r);
        double normF = normVector(f);
        double ro = normR / normF;
        printResults(normZ, zeta, normR, ro);
    }

    private static void printResults(double z, double zeta, double r, double ro) {
        System.out.println("||Z|| = " + z);
        System.out.println("zeta = " + zeta);
        System.out.println("||R|| = " + r);
        System.out.println("ro = " + ro);
    }
}
