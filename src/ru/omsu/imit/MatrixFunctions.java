package ru.omsu.imit;

import java.util.Arrays;

import static java.lang.Math.*;

public class MatrixFunctions {

    /**
     * Преобразование матрицы к верхнетреугольному виду.
     */
    public static double[][] triangleMatrix(double[][] arr) {
        for (int k = 0; k < arr.length; k++) {
            for (int i = k + 1; i < arr.length; i++) {
                double mu = arr[i][k] / arr[k][k];
                for (int j = 0; j < arr.length; j++) {
                    arr[i][j] -= arr[k][j] * mu;
                }
            }
        }
        return arr;
    }

    /**
     * Векторное вычитание
     */
    public static double[] vectorSub(double[] a, double[] b) {
        int size = a.length;
        double[] c = new double[size];
        for (int i = 0; i < size; i++) {
            c[i] = a[i] - b[i];
        }
        return c;
    }

    /**
     * Умножение матрицы на вектор
     */
    public static double[] matrixVectorMul(double[][] matrix, double[] vector) {
        int size = matrix.length;
        double[] c = new double[size];
        for (int i = 0; i < size; i++) {
            c[i] = 0;
            for (int j = 0; j < size; j++) {
                c[i] += matrix[i][j] * vector[j];
            }
        }
        return c;
    }

    /**
     * Векторная норма
     */
    public static double normVector(double[] vector) {
        double quadSum = 0;
        for (double vx : vector) {
            quadSum = max(abs(quadSum), abs(vx));
        }
        return quadSum;
    }

    /**
     * Матричная норма
     */
    public static double normMatrix(double[][] a) {
        int size = a.length;
        double temp = 0;
        for (double[] row : a) {
            temp += Math.abs(row[0]);
        }
        for (int i = 1; i < size; i++) {
            double t = 0;
            for (int j = 0; j < size; j++) {
                t += Math.abs(a[i][j]);
            }
            if (temp < t && Math.abs(temp - t) < 1e-9) {
                temp = t;
            }
        }
        return temp;
    }

    /**
     * Расчет ошибки
     */
    public static double[] countError(double[] a, double[] b) {
        int size = a.length;
        double[] c = new double[size];
        for (int i = 0; i < size; i++) {
            c[i] = a[i] - b[i];
        }
        return c;
    }

    /**
     * Расчет невязки A * x - F
     */
    public static double[] countResidual(double[][] a, double[] x, double[] f) {
        double[] ax = matrixVectorMul(a, x);
        double[] res = vectorSub(ax, f);

        return res;
    }

    public static void printMatrix(double[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            System.out.print(Arrays.toString(matrix[i]));
            if (i != matrix.length - 1) {
                System.out.println(",");
            }
        }
    }

    public static void printVector(double[] vector) {
        System.out.println(Arrays.toString(vector));
    }

    /**
     * Создание единичной матрицы
     * @param size - размер
     */
    public static double[][] createUnitMatrix(int size) {
        double[][] matrix = new double[size][size];
        for (int i = 0; i < size; i++) {
            matrix[i][i] = 1;
        }
        return matrix;
    }

    public static double[] copyVector(double[] vector) {
        return Arrays.copyOf(vector, vector.length);
    }

    public static double[][] copyMatrix(double[][] arr) {
        int size = arr.length;
        double[][] copy = new double[size][size];
        for (int i = 0; i < size; i++) {
            copy[i] = Arrays.copyOf(arr[i], size);
        }
        return copy;
    }

    public static void showMatrix(double[][] A) {
        System.out.println("||A|| = " + normMatrix(A));
    }
}
