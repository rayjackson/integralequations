import static java.lang.Math.*;

public class Do {
    public double func(double x,double l){
        double f=sin(PI*x)-2./PI*l;
        return f;
    }

    public double kernel(double x,double s){
        double f =1;
        return f;
    }


    public double[] integ(double lamda,int n,double eps){
        double[] y=new double[n];
        double[][] a=new double[n][n];
        double[] b=new double[n];
        double[] xser=new double[n];
        double h=1./n;
        double lh=lamda*h;
        for (int i = 0; i <n ; i++) {
            xser[i]=(i+0.5)*h;
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j <n; j++) {
                a[i][j]=-lh*kernel(xser[j],xser[j]);

            }
            a[i][i]++;

            b[i]=func(xser[i],lamda);
        }
        iterMethod(y,a,b,n,eps);
        return y;
    }

    public boolean isStop(double[] x,double[] p,double eps){
        double max = 0;
        int n=x.length;
        for (int i = 0; i < n; i++)
        {
            max=max(abs(p[i]-x[i]),max);
        }
        //System.out.println("max="+max);
        if(max>=eps)
            return false;
        return true;
    }
    public void iterMethod(double[] x,double[][]a,double[] b,int n ,double eps){
        double[] p=new double[n];
        for (int i = 0; i < n; i++) {
            p[i] = x[i];
        }
        /*for (int i = 0; i <n ; i++) {
            for (int j = 0; j <n ; j++) {
                if(!(i ==j)){
                a[i][j]=a[i][j]/a[i][i];
                b[i]=b[i]/a[i][i];
                }
            }a[i][i]=a[i][i]/a[i][i];
        }*/
        long count=1;
        for (int i = 0; i < n; i++)
        {
            double var = 0;
            for (int j = 0; j < i; j++)
                var += (a[i][j] * x[j]);
            for (int j = i + 1; j < n; j++)
                var += (a[i][j] * p[j]);
            x[i] = (b[i] - var) / a[i][i];

        }


        while (!isStop(x,p,eps)){
            for (int i = 0; i < n; i++) {
                p[i] = x[i];
            }
            for (int i = 0; i < n; i++)
            {
                double var = 0;
                for (int j = 0; j < i; j++)
                    var += (a[i][j] * x[j]);
                for (int j = i + 1; j < n; j++)
                    var += (a[i][j] * p[j]);
                x[i] = (b[i] - var) / a[i][i];

            }
            count++;
            if(count>1000000)
                break;
        }

        System.out.println("iter="+count);
    }

    public void printv(double[] a){
        for (int i = 0; i <a.length ; i++) {
            System.out.println(a[i]+" ");

        }
    }
    public void print(double[][] a){
        for (int i = 0; i <a.length ; i++) {
            for (int j = 0; j < a.length; j++) {
                System.out.print(a[i][j]+" ");
            }
            System.out.println();
        }
    }
}